Use a static server to serve your output json files. Build a simple frontend app, where you make a HTTP request to get the json file and display the data as a visualization. The visualization should be done using a library called highcharts.js


Directory structure:

- src/
  - client/
    - index.html
    - app.js
    - app.css
  - server/
    - ipl.js
    - index.js
  - output/
    - matchesPerYear.json
  - data/
    - matches.csv
    - deliveries.csv
- package.json
- .gitignore


1. Create a client folder
2. Write the JS code to get the data, do not use jQuery
3. Run a static server in this folder
