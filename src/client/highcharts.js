const matchesPerYear = () => {
    fetch('http://localhost:2000/matchesPerYear.json')
        .then(response => {
            return response.json();
        })
        .then(obj => {
            Highcharts.chart('container', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Matches Per Year'
                },
                yAxis: {
                    title: {
                        text: 'Number of Matches Played'
                    }
                },
                xAxis: {
                    categories: Object.keys(obj),
                    title: {
                        text: 'Matches played Seasons'
                    }
                },
                series: [{
                    name: 'IPL Matches',
                    data: Object.values(obj),
                    color: '#85929E',
                    dataLabels: {
                        enabled: true,
                        color: '#FFFFFF',
                        align: 'right',
                        y: 15,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    },
                }],
            });
        });
}


const matchesWonPerYear = () => {
    fetch('http://localhost:2000/matchesWonPerYear.json')
        .then(response => {
            return response.json();
        })
        .then(obj => {
            let val = Object.values(obj)

            let RCB = val.reduce((res, data) => {
                res.push(data["Royal Challengers Bangalore"]);
                return res;
            }, []);
            let SRH = val.reduce((res, data) => {
                if (data["Sunrisers Hyderabad"]) res.push(data["Sunrisers Hyderabad"]);
                else if (data["Deccan Chargers"]) res.push(data["Deccan Chargers"]);
                else res.push(0);
                return res;
            }, []);
            let KKR = val.reduce((res, data) => {
                res.push(data["Kolkata Knight Riders"]);
                return res;
            }, []);
            let CSK = val.reduce((res, data) => {
                res.push(data["Chennai Super Kings"]);
                return res;
            }, []);
            let RPS = val.reduce((res, data) => {
                if (data["Pune Warriors"]) res.push(data["Pune Warriors"]);
                else if (data["Rising Pune Supergiants"]) res.push(data["Rising Pune Supergiants"]);
                else if (data["Rising Pune Supergiant"]) res.push(data["Rising Pune Supergiant"]);
                else res.push(0);
                return res;
            }, []);
            let DD = val.reduce((res, data) => {
                res.push(data["Delhi Daredevils"]);
                return res;
            }, []);
            let RR = val.reduce((res, data) => {
                if (data["Rajasthan Royals"]) res.push(data["Rajasthan Royals"]);
                else res.push(0);
                return res;
            }, []);
            let KXIP = val.reduce((res, data) => {
                if (data["Kings XI Punjab"]) res.push(data["Kings XI Punjab"]);
                else res.push(0);
                return res;
            }, []);
            let MI = val.reduce((res, data) => {
                if (data["Mumbai Indians"]) res.push(data["Mumbai Indians"]);
                else res.push(0);
                return res;
            }, []);
            let KTK = val.reduce((res, data) => {
                if (data["Kochi Tuskers Kerala"]) res.push(data["Kochi Tuskers Kerala"]);
                else res.push(0);
                return res;
            }, []);
            // let noName = val.reduce((res, data) => {
            //     if (data[""]) res.push(data[""]);
            //     else res.push(0);
            //     return res;
            // }, []);

            Highcharts.chart('container', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Matches Won Per Year'
                },
                xAxis: {
                    categories: Object.keys(obj),
                    title: {
                        text: 'Matches played Seasons'
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: '10'
                    }
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                        name: "Kolkata Knight Riders",
                        data: KKR,
                    }, {
                        name: "Chennai Super Kings",
                        data: CSK
                    }, {
                        name: "Delhi Daredevils",
                        data: DD
                    }, {
                        name: "Royal Challengers Bangalore",
                        data: RCB
                    }, {
                        name: "Rajasthan Royals",
                        data: RR
                    }, {
                        name: "Kings XI Punjab",
                        data: KXIP
                    }, {
                        name: "Sunrisers Hyderabad",
                        data: SRH
                    }, {
                        name: "Mumbai Indians",
                        data: MI
                    }, {
                        name: 'Pune Warriors',
                        data: RPS
                    }, {
                        name: 'Kochi Tuskers Kerala',
                        data: KTK
                    },
                    // {
                    //     name: 'Anonymous',
                    //     data: noName
                    // }
                ]
            });
        });

}

const extraRunConceed = () => {
    fetch('http://localhost:2000/extraRunConceedPerTeam2016.json')
        .then(response => {
            return response.json();
        })
        .then(obj => {
            Highcharts.chart('container', {
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'Extra Run Conceeded Per Team in 2016'
                },
                yAxis: {
                    title: {
                        text: 'Number of Extra Runs conceeded'
                    }
                },
                xAxis: {
                    categories: Object.keys(obj),
                    title: {
                        text: '2016 Season Teams'
                    },
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle'
                },
                series: [{
                    name: 'Extra Runs',
                    data: Object.values(obj),
                    dataLabels: {
                        enabled: true,
                        rotation: 0,
                        color: '#FFFFFF',
                        align: 'right',
                        format: '{point.y:.0f}',
                        y: 0,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    },
                }],
            });
        });
}


const topBowler = () => {
    fetch('http://localhost:2000/top10EconomicBowler.json')
        .then(response => {
            return response.json();
        })
        .then(obj => {
            Highcharts.chart('container', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Top 10 Economic Bowlers in 2015'
                },
                yAxis: {
                    title: {
                        text: 'Economy Per Bowler'
                    }
                },
                xAxis: {
                    categories: Object.keys(obj),
                    title: {
                        text: '2015 Top Bowlers'
                    },
                    type: 'category',
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                series: [{
                    name: 'Economy',
                    data: Object.values(obj),
                    dataLabels: {
                        enabled: true,
                        rotation: 0,
                        color: '#FFFFFF',
                        align: 'right',
                        format: '{point.y:.2f}',
                        y: 10,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    },
                    color: '#0A94B3'
                }],
            });
        });
}

// matchesPerYear();
// matchesWonPerYear();
// extraRunConceed();
// topBowler();