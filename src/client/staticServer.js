const http = require('http');
const fs = require('fs');


http.createServer((req, res) => {

    switch (req.url) {
        case '/':
            fs.readFile('./index.html', 'UTF-8', (err, data) => {
                if (err) throw err;
                else res.end(data);
            });
            break;

        case '/highcharts.js':
            fs.readFile('./highcharts.js', (err, data) => {
                if (err) throw err;
                else res.end(data);
            });
            break;

        case '/matchesPerYear.json':
            fs.readFile('./../output/matchesPerYear.json', 'UTF-8', (err, data) => {
                if (err) throw err;
                else res.end(data);
            });
            break;

        case '/matchesWonPerYear.json':
            fs.readFile('./../output/matchesWonPerYear.json', 'UTF-8', (err, data) => {
                if (err) throw err;
                else res.end(data);
            });
            break;

        case '/extraRunConceedPerTeam2016.json':
            fs.readFile('./../output/extraRunConceedPerTeam2016.json', 'UTF-8', (err, data) => {
                if (err) throw err;
                else res.end(data);
            });
            break;

        case '/top10EconomicBowler.json':
            fs.readFile('./../output/top10EconomicBowler.json', 'UTF-8', (err, data) => {
                if (err) throw err;
                else res.end(data);
            });
            break;

            // case '/app.css':
            //     fs.readFile('./app.css', 'UTF-8', (err, data) => {
            //         if (err) throw err;
            //         else res.end(data);
            //     });
            //     break;

        default:
            res.writeHead(200, { 'Content-Type': 'text/html' });
            res.write('Error: Files not found');
            res.end();
            break;

    }
}).listen(2000, () => {
    console.log('Static Server created on port 2000!');
});