const matchesJson = require('./../data/matches.json');
const deliveriesJson = require('./../data/deliveries.json');
const fs = require('fs');


const getMatchesPerYear = (matchesObj) => {
    let matchesArr = Object.entries(matchesObj);
    let matchesPerYear = matchesArr.map((matchesEntry) => {
        let matchesPerYear = {};
        if (matchesPerYear[matchesEntry[1].season]) {
            matchesPerYear[matchesEntry[1].season]++;
        } else {
            matchesPerYear[matchesEntry[1].season] = 1;
        }
        return matchesPerYear;
    })
    return matchesPerYear[0];
}


const getMatchesWonPerYear = (matchesObj) => {
    let matchesArr = Object.entries(matchesObj);
    let result = {};
    let matchesWonPerYear = matchesArr.map((matchesEntry) => {
        if (result[matchesEntry[1].season] === undefined) {
            result[matchesEntry[1].season] = {};
        }
        if (result[matchesEntry[1].season][matchesEntry[1].winner]) {
            result[matchesEntry[1].season][matchesEntry[1].winner]++;
        } else {
            result[matchesEntry[1].season][matchesEntry[1].winner] = 1;
        }
        return result;
    })
    return matchesWonPerYear[0];
}


const outputFile = (filePath, matchesPerYear) => {
    const input = JSON.stringify(matchesPerYear, null, 2);
    const output = '../output/' + filePath;
    fs.writeFile(output, input, () => {
        console.log(`Data saved into "${output}"`);
    });
}


// const matchesPerYear = getMatchesPerYear(matchesJson);
// outputFile('matchesPerYear.json', matchesPerYear);
// console.log(matchesPerYear);

// const matchesWonPerYear = getMatchesWonPerYear(matchesJson);
// outputFile('matchesWonPerYear.json', matchesWonPerYear);
// console.log(matchesWonPerYear);