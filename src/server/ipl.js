let csvTojson = require('convert-csv-to-json');
let fs = require('fs');

let matchesJson = csvTojson.getJsonFromCsv('/home/dev/IPL-Data-Project-I/src/data/matches.csv');
let deliveriesJson = csvTojson.getJsonFromCsv('/home/dev/IPL-Data-Project-I/src/data/deliveries.csv');


function matchesPerYear(matches) {
    let obj = {};
    for (let i in matches) {
        if (obj[matches[i].season]) {
            obj[matches[i].season]++;
        }
        else {
            obj[matches[i].season] = 1;
        }
    }
    fs.appendFile ('/home/dev/IPL-Data-Project-I/src/output/matchesPerYear.json', '', () => {
        console.log("matchesPerYear.json file created")
    });
    let file = JSON.stringify(obj, null, 2);
    fs.writeFile ('/home/dev/IPL-Data-Project-I/src/output/matchesPerYear.json', file, () => {
        console.log("Data copied");
    });
    return obj;
}

function matchesWonPerYear(matches) {
    let obj = {};
    for (let i in matches) {
        if(obj[matches[i].season]) {
            if(obj[matches[i].season][matches[i].winner]) {
                obj[matches[i].season][matches[i].winner]++;
            }
            else {
                obj[matches[i].season][matches[i].winner] = 1;
            }
        }
        else {
            obj[matches[i].season] = {};
        }
    }
    fs.appendFile ('/home/dev/IPL-Data-Project-I/src/output/matchesWonPerYear.json', '', () => {
        console.log('matchesWonPerYear.json file created')
    })
    let file = JSON.stringify(obj, null, 2);
    fs.writeFile ('/home/dev/IPL-Data-Project-I/src/output/matchesWonPerYear.json', file, () => {
        console.log("Data copied");
    })
    return obj;
}

function extraRunConceedPerTeam2016(matches,deliveries) {
    let obj = {};
    for (let i in matches) {
        for (let j in deliveries) {
            if(matches[i].season === '2016') {
                if(matches[i].id === deliveries[j].match_id) {
                    if(obj[deliveries[j].bowling_team]) {
                        obj[deliveries[j].bowling_team] = parseInt (obj[deliveries[j].bowling_team]) + parseInt (deliveries[j].extra_runs);
                    }
                    else {
                        obj[deliveries[j].bowling_team] = deliveries[j].extra_runs;
                    }
                }
            }
        }
    }
    fs.appendFile ('/home/dev/IPL-Data-Project-I/src/output/extraRunConceedPerTeam2016.json', '' ,() => {
        console.log('extraRunConceedPerTeam2016.json file created');
    });
    let file = JSON.stringify(obj, null, 2);
    fs.writeFile ('/home/dev/IPL-Data-Project-I/src/output/extraRunConceedPerTeam2016.json', file, () => {
        console.log('Data copied');
    });
    return obj;
}

function top10EconomicBowler(matches,deliveries) {
    let totalRuns = {};
    let overCount = {};
    for (let i in matches) {
        for (let j in deliveries) {
            if (matches[i].season === '2015') {
                if (matches[i].id === deliveries[j].match_id) {
                    if(totalRuns[deliveries[j].bowler]) {
                        totalRuns[deliveries[j].bowler] = parseInt (totalRuns[deliveries[j].bowler]) + parseInt (deliveries[j].total_runs) - parseInt (deliveries[j].bye_runs) - parseInt (deliveries[j].legbye_runs);;
                    }
                    else {
                        totalRuns[deliveries[j].bowler] = parseInt (deliveries[j].total_runs) - parseInt (deliveries[j].bye_runs) - parseInt (deliveries[j].legbye_runs);
                    }
                    if (deliveries[j].wide_runs === '0' && deliveries[j].noball_runs === '0') {
                        if (overCount[deliveries[j].bowler]) {
                            overCount[deliveries[j].bowler]++;
                        }
                        else {
                            overCount[deliveries[j].bowler] = 1;
                        }
                    }
                }
            }
        }
    }
    let over = Object.values(overCount);
    let balls = Object.values(totalRuns);
    let res = [];
    for (let i in over) {
        res[i] = Math.round ((balls[i] / (over[i] / 6)) * 100) /100;
    }
    let overValue = Object.keys(overCount);
    let resObj = {};
    for (let i = 0; i < overValue.length; i++) {
        resObj[overValue[i]] = res[i];
    }
    let arr = Object.entries(resObj).sort((i, j) => i[1] - j[1]);
    let obj = {};
    for (let i = 0; i < 10; i++) {
        obj[arr[i][0]] = arr[i][1];
    }
    fs.appendFile('/home/dev/IPL-Data-Project-I/src/output/top10EconomicBowler.json', '', () => {
        console.log('top10EconomicBowler.json file created');
    })
    let file = JSON.stringify(obj, null, 2);
    fs.writeFile('/home/dev/IPL-Data-Project-I/src/output/top10EconomicBowler.json', file, () => {
        console.log('Data copied');
    })
    return obj;
}

// console.log(matchesPerYear(matchesJson));
// console.log(matchesWonPerYear(matchesJson));
// console.log(extraRunConceedPerTeam2016(matchesJson,deliveriesJson));
console.log(top10EconomicBowler(matchesJson,deliveriesJson));