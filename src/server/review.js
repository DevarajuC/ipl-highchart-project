const fs = require('fs');

const file1 = () => {
    return new Promise((resolve, reject) => {
            fs.readFile('./../../package.json', 'UTF-8', (err, data) => {
                if (err) reject(err);
                else {
                    resolve(data);
                }
            })
        })
        .catch((err) => console.log(err));
}

const file2 = () => {
    return new Promise((resolve, reject) => {
            fs.readFile('./../../package-lock.json', 'UTF-8', (err, data) => {
                if (err) reject(err);
                else {
                    resolve(data);
                }
            })
        })
        .catch(err => console.log(err))
}

// Promise.all([file1(), file2()])
//     .then(res => fs.writeFile('./result.json', res, (err) => {
//         if (err) return err;
//         else {
//             console.log(res);
//             console.log('Data copied');
//         }
//     }));




const asyncRes = async() => {
    const res = await Promise.all([file1(), file2()]);
    fs.writeFile('./result.json', res, (err) => {
        if (err) return err;
        else {
            console.log(res);
            console.log('Data copied');
        }
    });
}
asyncRes();